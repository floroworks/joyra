<?php

namespace Bandit\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RedirectsUsers;

trait BanditRegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        //dd ('bandit registers users');
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());


        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        if (Auth::user()) {
            $this->create($request->all());
            return redirect('/admin/user');
        } else {
            Auth::login($this->create($request->all()));
        }

        return redirect($this->redirectPath());
    }
}
