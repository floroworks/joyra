<?php namespace Bandit\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

trait BanditAuthenticatesAndRegistersUsers
{
    use AuthenticatesUsers, BanditRegistersUsers {
        AuthenticatesUsers::redirectPath insteadof BanditRegistersUsers;
    }
}