<?php namespace Bandit\Repositories;

interface ActionEditableFieldRepositoryInterface
{
    public function getEditableFieldsForAction($action);
}