<?php namespace Bandit\Repositories;

trait ForeignKeyRetriever {

    protected function assigned_to($id)
    {
        return \App\User::find($id)->name;
    }

    protected function created_by($id)
    {
        return $this->assigned_to($id);
    }

    protected function project_id($id)
    {
        return \App\Project::find($id)->name;
    }

    protected function issue_type_id($id)
    {
        return \App\IssueType::find($id)->type;
    }

    protected function issue_status_type_id($id)
    {
        return \App\IssueStatusType::find($id)->status;
    }

    protected function issue_resolution_type_id($id)
    {
        return \App\IssueResolutionType::find($id)->type;
    }

    protected function priority_id($id)
    {
        return \App\PriorityType::find($id)->priority;
    }

    protected function severity_id($id)
    {
        return \App\SeverityType::find($id)->severity;
    }

}