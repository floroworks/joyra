<?php namespace Bandit\Repositories;

interface UserRepositoryInterface
{
    public function getAllUsersAsListOrderedBy($key, $showhidden);

    public function getAllUsers($showhidden);

    public function getAllUsersWith(array $these_columns, $showhidden);

    public function getUserById($id);

    public function getActiveIssues();
}