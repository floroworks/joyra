<?php namespace Bandit\Repositories;

use App\Project;
use App\ProjectsCounter;

class EloquentProjectRepository implements ProjectRepositoryInterface
{
    public function save(array $id, $name, $slug, $user_id)
    {
        $project = Project::updateOrCreate($id,
            ['name' => $name, 'slug' => $slug, 'user_id' => $user_id]
        );
    }

    public function getProjectsAsList()
    {
        return Project::orderBy('name')->lists('name', 'id');
    }

    public function getProjectsWithProjectLeads()
    {
        return Project::with('projectLead')->get();
    }
}