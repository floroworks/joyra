<?php namespace Bandit\Repositories;

use App\IssueStatusType;

class EloquentWorkflowRepository implements WorkflowRepositoryInterface
{
    public function getActionsAvailableFor($id)
    {
        //dd(IssueStatusType::find(1));
        return IssueStatusType::find($id)->actions;
    }
}