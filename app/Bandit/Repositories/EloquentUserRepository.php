<?php namespace Bandit\Repositories;

use App\User;

class EloquentUserRepository implements UserRepositoryInterface
{

    public function getAllUsersAsListOrderedBy($key = 'name', $showhidden = false)
    {
        if ($showhidden) {
            return User::orderBy($key)->lists('name', 'id');
        } else {
            return User::where('hidden', false)->orderBy($key)->lists('name', 'id');
        }
    }

    public function getAllUsers($showhidden = false)
    {
        return User::all();
    }

    public function getAllUsersWith(array $these_columns, $showhidden = false)
    {
        return User::with($these_columns)->get();
    }

    public function getUserById($id)
    {
        
        //dd(User::with('roles')->find($id));
        return User::with('roles')->find($id);
        //return User::find($id);
    }

    public function getActiveIssues()
    {
        $this->issues();
    }
}