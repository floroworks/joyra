<?php namespace Bandit\Repositories;

interface IssuesRepositoryInterface
{
    public function save(array $these_columns);
    public function getSlugOf(\App\Issue $issue);
    public function getIssueTypesAsList();
    public function getIssueStatusTypesAsList();
    public function getAllIssuesWith(array $these_columns);
    public function getThisIssueWith($key, array $these_columns);
    public function getIssueResolutionTypesAsList();
    public function touchIssueWithIdOf($id);
    public function getPriorityTypesAsList();
    public function getSeverityTypesAsList();
}