<?php namespace Bandit\Repositories;

interface WorkflowRepositoryInterface
{
    public function getActionsAvailableFor($id);
}