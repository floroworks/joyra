<?php namespace Bandit\Repositories;

interface ProjectRepositoryInterface
{
    public function save(array $id, $name, $slug, $user_id);
    public function getProjectsAsList();
    public function getProjectsWithProjectLeads();
}