<?php namespace Bandit\Repositories;

use App\StatusAction;

class EloquentActionEditableFieldRepository implements ActionEditableFieldRepositoryInterface
{
    public function getEditableFieldsForAction($action)
    {
        return StatusAction::find($action)->editableFields;
    }
}