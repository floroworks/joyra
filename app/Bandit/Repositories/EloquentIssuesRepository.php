<?php namespace Bandit\Repositories;

use App\IssueType;
use App\Issue;
use App\IssueStatusType;
use App\IssueResolutionType;
use App\PriorityType;
use App\SeverityType;

class EloquentIssuesRepository implements IssuesRepositoryInterface
{
    public function save(array $these_columns)
    {
        $slug = array('slug' => '');
        if (isset($these_columns['slug']) || 
                array_key_exists('slug', $these_columns)) {
            $slug = array('slug' => $these_columns['slug']);
            unset($these_columns['slug']);
        }

        return Issue::updateOrCreate($slug, $these_columns);
    }

    public function getSlugOf(\App\Issue $issue)
    {
        return Issue::where('id', $issue->getKey())
                        ->first()
                        ->getAttributeValue('slug');
    }

    public function getPriorityTypesAsList()
    {
        return PriorityType::lists('priority', 'id');
    }

    public function getSeverityTypesAsList()
    {
        return SeverityType::lists('severity', 'id');
    }

    public function getIssueTypesAsList()
    {
        return IssueType::lists('type', 'id');
    }

    public function getIssueStatusTypesAsList()
    {
        return IssueStatusType::lists('status', 'id');
    }

    public function getAllIssuesWith(array $these_columns)
    {
        return Issue::with($these_columns)->get();
    }

    public function getThisIssueWith($key, array $these_columns)
    {
        return Issue::with($these_columns)
            ->where('slug', '=', $key)
            ->first();
    }

    public function getIssueResolutionTypesAsList()
    {
        return IssueResolutionType::lists('type', 'id');
    }

    public function touchIssueWithIdOf($id)
    {
        Issue::find($id)->touch();
    }

}