<?php namespace Bandit\Repositories;

use App\Comment;

class EloquentCommentRepository implements CommentRepositoryInterface
{
    public function save(array $id, array $these_columns)
    {
        $comment = Comment::updateOrCreate($id, $these_columns);

        return $comment;
    }

    public function getThisComment($id)
    {
        return Comment::find($id);
    }
}