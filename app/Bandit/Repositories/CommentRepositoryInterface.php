<?php namespace Bandit\Repositories;

interface CommentRepositoryInterface
{
    public function save(array $id, array $these_columns);
    public function getThisComment($id);
}