<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //\App\Issue::created(\App\Listeners\IssueEventRecorder::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Project Repository Interface
        $this->app->bind(
            'Bandit\Repositories\ProjectRepositoryInterface',
            'Bandit\Repositories\EloquentProjectRepository');
        // User Repository Interface
        $this->app->bind(
            'Bandit\Repositories\UserRepositoryInterface',
            'Bandit\Repositories\EloquentUserRepository');
        // Issues Repository Interface
        $this->app->bind(
            'Bandit\Repositories\IssuesRepositoryInterface', 
            'Bandit\Repositories\EloquentIssuesRepository');
        // Comment Repository Interface
        $this->app->bind(
            'Bandit\Repositories\CommentRepositoryInterface', 
            'Bandit\Repositories\EloquentCommentRepository');
        // Workflow Repository Interface
        $this->app->bind(
            'Bandit\Repositories\WorkflowRepositoryInterface', 
            'Bandit\Repositories\EloquentWorkflowRepository');
        // Action Editable Fields Interface
        $this->app->bind(
            'Bandit\Repositories\ActionEditableFieldRepositoryInterface', 
            'Bandit\Repositories\EloquentActionEditableFieldRepository');
    }
}
