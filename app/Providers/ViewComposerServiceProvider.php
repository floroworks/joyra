<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('projects.index', 'App\Http\ViewComposers\ProjectComposer');
        view()->composer(['projects.create', 'projects.edit', 'issues.create', 'issues.show', 'issues.edit'], 'App\Http\ViewComposers\UserComposer');
        view()->composer(['issues.create', 'issues.edit', 'issues.show'], 'App\Http\ViewComposers\IssueComposer');
        view()->composer(['issues.show'], 'App\Http\ViewComposers\WorkflowComposer');
        view()->composer(['admin.user.index'], 'App\Http\ViewComposers\AdminUserComposer');
        view()->composer('admin.workflows.statusactions', 'App\Http\ViewComposers\Admin\AdminIssueActionsComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
