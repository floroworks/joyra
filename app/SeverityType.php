<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeverityType extends Model
{

    //protected $table = 'severity_types';

    public function issues() {
        return $this->hasMany('App\Issue');
    }
}
