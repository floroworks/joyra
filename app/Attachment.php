<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    public function issue()
    {
        return $this->belongsTo('\App\Issue', 'issue_id');
    }
}
