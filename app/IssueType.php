<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IssueType extends Model
{
    protected $fillable = ['type', 'description'];

    public function issues()
    {
        return $this->hasMany('App\Issue');
    }
}
