<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'history';

    public function issues()
    {
        return $this->belongsTo('\App\Issue', 'issue_id');
    }

    public function changedBy()
    {
        return $this->belongsTo('\App\Users', 'changed_by');
    }
}
