<?php

namespace App\Listeners;

use App\Events\IssueCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Auth;

use Bandit\Repositories\UserRepositoryInterface;
use Bandit\Repositories\ProjectRepositoryInterface;

use Bandit\Repositories\ForeignKeyRetriever;

class IssueEventRecorder
{
    use ForeignKeyRetriever;

    protected $history;
    protected $user;
    protected $user_int;
    protected $project_int;

    private $ISSUE_COLUMNS = ['subject' => 'Subject',
                                    'description' => 'Description',
                                    'slug' => 'Issue Key',
                                    'created_by' => 'Reporter',
                                    'assigned_to' => 'Assignee',
                                    'project_id' => 'Project',
                                    'issue_type_id' => 'Issue Type',
                                    'issue_status_type_id' => 'Status',
                                    'issue_resolution_type_id' => 'Resolution',
                                    'priority_id' => 'Priority',
                                    'severity_id' => 'Severity',
                                   ];
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(\App\History $history, 
                                UserRepositoryInterface $user_int,
                                ProjectRepositoryInterface $project_int) 
    {
        $this->history = $history;
        $this->user_int = $user_int;
        $this->project_int = $project_int;
    }


    public function onIssueCreated($event)
    {
        $message = Auth::user()->name . ' created a new issue "';
        $message .= $event->subject . '"';

        $this->history->issue_id = $event->id;
        $this->history->change_description = $message;
        $this->history->changed_by = Auth::user()->id;
        $this->history->save();
    }

    public function onIssueUpdated($event)
    {
        //dd($event->getDirty());

        $dirty = $event->getDirty();
        $updated_at = '';
        if (array_key_exists('updated_at', $dirty)) {
            $updated_at = $dirty['updated_at'];
            unset($dirty['updated_at']);
        }

        $changes = array();
        foreach($dirty as $attribute => $value) {
            $changes[] = ['column' => $attribute, 'original' => $event->getOriginal($attribute), 'modified' => $value];
        }



        $message = Auth::user()->name . ' changed ';
        foreach($changes as $change) {

            if (method_exists($this, $change['column'])) {
                $change['original'] = $this->{$change['column']}($change['original']);
                $change['modified'] = $this->{$change['column']}($change['modified']);
            }

            $message .= $this->ISSUE_COLUMNS[$change['column']] . ' from "' . $change['original'] . '" to "' . $change['modified'] . '" ';
        }

        $message .= ' at ' . $updated_at;

        //dd($message);
        $this->history->issue_id = $event->id;
        $this->history->change_description = $message;
        $this->history->changed_by = Auth::user()->id;
        $this->history->save();

    }

    public function subscribe($events)
    {
        $events->listen(
            'issue.created',
            'App\Listeners\IssueEventRecorder@onIssueCreated'
        );

        $events->listen(
            'issue.updated',
            'App\Listeners\IssueEventRecorder@onIssueUpdated'
        );
    }


}
