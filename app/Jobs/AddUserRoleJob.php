<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

use App\User;

class AddUserRoleJob extends Job implements SelfHandling
{

    protected $user;
    protected $roles;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, array $roles = null)
    {
        //dd($user);
        $this->user = $user;
        if (is_null($roles)) {
            $this->roles = [];
        } else {
            $this->roles = $roles;
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //dd($this->user);
        $this->user->roles()->sync($this->roles);

        //dd($this->user->roles());
    }
}
