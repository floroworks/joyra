<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

use App\Events\IssueCreatedEvent;
use App\Events\IssueUpdatedEvent;

use Event;

use Bandit\Repositories\IssuesRepositoryInterface;

class CreateIssueJob extends Job implements SelfHandling
{
    protected $id;
    protected $project_id;
    protected $issue_type_id;
    protected $issue_status_type_id;
    protected $subject;
    protected $created_by;
    protected $assigned_to;
    protected $description;
    protected $issue_resolution_type_id;
    protected $slug;
    protected $priority_id;
    protected $severity_id;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id = null, 
                                $project_id = null, 
                                $issue_type_id = null, 
                                $issue_status_type_id = null, 
                                $subject = null, 
                                $created_by = null, 
                                $assigned_to = null, 
                                $description = null, 
                                $issue_resolution_type_id = null, 
                                $slug = null,
                                $priority_id = null,
                                $severity_id = null)
    {
        $this->id = $id;
        $this->project_id = $project_id;
        $this->issue_type_id = $issue_type_id;
        $this->issue_status_type_id = $issue_status_type_id;
        $this->subject = $subject;
        $this->created_by = $created_by;
        $this->assigned_to = $assigned_to;
        $this->description = $description;
        $this->issue_resolution_type_id = $issue_resolution_type_id;
        //$this->slug = (isset($slug)) ? $slug : null;
        $this->slug = $slug;
        $this->priority_id = $priority_id;
        $this->severity_id = $severity_id;
        //dd($this);

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(IssuesRepositoryInterface $repository)
    {
        // dd($this->event);
        $columns = array();
        $changed_columns = array();
        $is_new = true;

        if (isset($this->subject)) {
          $columns['subject'] = $this->subject;
          $changed_columns['Subject'] = $this->subject;
        }

        if (isset($this->description)) {
          $columns['description'] = $this->description;
          $changed_columns['Description'] = $this->description;
        }

        if (isset($this->created_by)) {
          $columns['created_by'] = $this->created_by;
          $changed_columns['Created By'] = $this->created_by;
        }

        if (isset($this->assigned_to)) {
          $columns['assigned_to'] = $this->assigned_to;
          $changed_columns['Assigned To'] = $this->assigned_to;
        }

        if (isset($this->project_id)) {
          $columns['project_id'] = $this->project_id;
          $changed_columns['Project'] = $this->project_id;
        }

        if (isset($this->issue_type_id)) {
          $columns['issue_type_id'] = $this->issue_type_id;
          $changed_columns['Issue Type'] = $this->issue_type_id;
        }

        if (isset($this->issue_status_type_id)) {
          $columns['issue_status_type_id'] = $this->issue_status_type_id;
          $changed_columns['Status'] = $this->issue_status_type_id;
        }

        if (isset($this->issue_resolution_type_id)) {
          $columns['issue_resolution_type_id'] = $this->issue_resolution_type_id;
          $changed_columns['Resolution'] = $this->issue_resolution_type_id;
        }
        //$columns['created_at'] = new \DateTime();

        if (isset($this->slug)) {
          // for edit since we have a slug
          $columns['slug'] = $this->slug;
          $is_new = false;
        } else {
          // new, no slug so updated created_at
          $columns['created_at'] = new \DateTime();
        }

        if (isset($this->priority_id)) {
          $columns['priority_id'] = $this->priority_id;
          $changed_columns['Priority'] = $this->priority_id;
        }

        if (isset($this->severity_id)) {
          $columns['severity_id'] = $this->severity_id;
          $changed_columns['Severity'] = $this->severity_id;
        }

        //dd($columns);

        $issue = $repository->save($columns);

        //Event::fire(new IssueCreatedEvent($issue));
        if ($is_new) {
          Event::fire(new IssueCreatedEvent($issue));
        } else {
          Event::fire(new IssueUpdatedEvent($issue, $changed_columns));
        }
        
        return $repository->getSlugOf($issue);
    }

}
