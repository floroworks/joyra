<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

use Illuminate\Support\Facades\Auth;

use Bandit\Repositories\CommentRepositoryInterface;
use Bandit\Repositories\IssuesRepositoryInterface;

class CreateCommentJob extends Job implements SelfHandling
{

    protected $id;
    protected $issue_id;
    protected $user_id;
    protected $comment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id = '', $issue_id, $comment)
    {
        $this->id = $id;
        $this->issue_id = $issue_id;
        $this->user_id = Auth::user()->id;
        $this->comment = $comment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CommentRepositoryInterface $repository, IssuesRepositoryInterface $issue_repo)
    {
        $id = ['id' => $this->id];
        $these_columns = ['issue_id' => $this->issue_id,
                          'user_id' => $this->user_id,
                          'comment' => $this->comment];

        //dd($these_columns);

        $comment_model = $repository->save($id, $these_columns);

        if (isset($comment_model)) {
            $issue_repo->touchIssueWithIdOf($this->issue_id);
        }
    }
}
