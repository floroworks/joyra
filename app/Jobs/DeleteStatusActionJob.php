<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class DeleteStatusActionJob extends Job implements SelfHandling
{

    protected $status_action;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\StatusAction $status_action)
    {
        $this->status_action = $status_action;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->status_action->editableFields()->sync([]);
        $this->status_action->delete();
    }
}
