<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class AddWorkflowJob extends Job implements SelfHandling
{

    protected $status;
    protected $actions;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\IssueStatusType $status, array $actions = null)
    {
        $this->status = $status;

        if (is_null($actions)){
            $this->actions = [];
        } else {
            $this->actions = $actions;
        }

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->status->actions()->sync($this->actions);
    }
}
