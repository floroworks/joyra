<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

use Bandit\Repositories\ProjectRepositoryInterface;

class CreateProjectJob extends Job implements SelfHandling
{
    protected $id;
    protected $name;
    protected $slug;
    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $name, $slug, $user_id)
    {
        $this->id   = $id;
        $this->name = $name;
        $this->slug = $slug;
        $this->user = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $repository)
    {
        //dd($this);
        $repository->save(array('id' => $this->id), $this->name, $this->slug, $this->user);
    }
}
