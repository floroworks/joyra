<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class AddEditableFieldsToActionJob extends Job implements SelfHandling
{

    protected $status_action;
    protected $editable_fields;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\StatusAction $status_action, array $editable_fields = null)
    {
        $this->status_action = $status_action;

        if (is_null($editable_fields)) {
            $this->editable_fields = [];
        } else {
            $this->editable_fields = $editable_fields;
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->status_action->editableFields()->sync($this->editable_fields);
    }
}
