<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IssueStatusType extends Model
{
    protected $fillable = ['status', 'description'];

    public function issues()
    {
        return $this->hasMany('App\Issue');
    }

    public function actions()
    {
        return $this->belongsToMany('App\StatusAction', 'workflows', 'current_status', 'available_action');
    }
}
