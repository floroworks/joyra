<?php

namespace App\Policies;

class CommentPolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function create($user)
    {
        if ($user->isAdmin() || $user->isDev() || $user->isUser()) {
            return true;
        }        
    }

    public function update($user, $comment)
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($comment->user->id == $user->id) {
            return true;
        }
    }

}
