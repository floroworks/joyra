<?php

namespace App\Policies;

use App\Issue;

class IssuePolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }


    public function create($user)
    {
        if ($user->isAdmin() || $user->isDev() || $user->isUser()) {
            return true;
        }
    }

    public function update($user, Issue $issue) 
    {
        if ($user->isAdmin() || $user->isDev()) {
            return true;
        } else if ($issue->created_by == $user->id) {
            return true;
        }
    } 
}
