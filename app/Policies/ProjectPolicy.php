<?php

namespace App\Policies;

use App\User;
use App\Project;

class ProjectPolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function create(User $user)
    {
        return $user->isAdmin();
    }

    public function update(User $user, Project $project)
    {
        return $user->isAdmin();
    }
}
