<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = ['issue_id', 'user_id', 'comment'];

    public function issue()
    {
        return $this->belongsTo('App\Issue', 'issue_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
