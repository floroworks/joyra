<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriorityType extends Model
{

    //protected $table = 'priority_types';

    public function issues()
    {
        return $this->hasMany('App\Issue');
    }
}
