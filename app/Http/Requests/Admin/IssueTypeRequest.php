<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class IssueTypeRequest extends Request
{

    protected $errorBag = 'admin_issue_type_request';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->route('issue_types'));
        $id = '';
        if (!is_null($this->route('issue_types'))) {
            $id = $this->route('issue_types')->id;
        }
        return [
            'type' => 'sometimes|required|max:30|unique:issue_types,type,' . $id,
        ];
    }
}
