<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class IssueResolutionTypeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = '';
        if (!is_null($this->route('issue_resolution_types'))) {
            $id = $this->route('issue_resolution_types')->id;
        }
        return [
            'type' => 'sometimes|required|max:30|unique:issue_resolution_types,type,' . $id,
        ];
    }
}
