<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class IssueStatusTypeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = '';
        if (!is_null($this->route('issue_status_types'))) {
            $id = $this->route('issue_status_types')->id;
        }
        return [
            'status' => 'sometimes|required|max:30|unique:issue_status_types,status,' . $id,
        ];
   }
}
