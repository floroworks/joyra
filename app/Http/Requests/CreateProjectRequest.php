<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//dd($this);
        return [
            'name' => 'required|min:3|max:50|unique:projects,name,' . $this->input('id'),
            'slug' => 'required|min:3|max:6|unique:projects,slug,' . $this->input('id'),
            'user_id' => 'required|integer|exists:users,id',
        ];
    }
}
