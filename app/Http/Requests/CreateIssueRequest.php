<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateIssueRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'sometimes|required|integer|exists:projects,id',
            'issue_type_id' => 'sometimes|required|integer|exists:issue_types,id',
            'subject' => 'sometimes|required|string',
            'created_by' => 'sometimes|required|integer|exists:users,id',
            'assigned_to' => 'sometimes|required|integer|exists:users,id',
            'description' => 'sometimes|required|string',
        ];
    }
}
