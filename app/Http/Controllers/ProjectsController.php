<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateProjectRequest;

use App\Project;
use App\Jobs\CreateProjectJob;
use Bandit\Repositories\UserRepositoryInterface;

class ProjectsController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('projects.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProjectRequest  $request
     * @return Response
     */
    public function store(CreateProjectRequest $request)
    {
        $this->dispatchFrom(CreateProjectJob::class, $request);

        return redirect()->route('projects.show', [$request->slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return Response
     */
    public function show(Project $project)
    {
        $matches = ['project_id' => $project->id, ];
        $issues = \App\Issue::where($matches)->
                              whereNotIn('issue_status_type_id', [5, 3])->
                              with('createdBy', 'assignedTo')->get();
        $issues_archived = \App\Issue::where($matches)->
                              whereIn('issue_status_type_id', [5, 3])->
                              with('createdBy', 'assignedTo')->get();
        return view('projects.show', compact('project', 'issues', 'issues_archived'));  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return Response
     */
    public function edit(Project $project)
    {
        return view('projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CreateProjectRequest  $request
     * @param  \App\Project  $project
     * @return Response
     */
    public function update(CreateProjectRequest $request, Project $project)
    {
        $this->dispatchFrom(CreateProjectJob::class, $request);

        return redirect()->route('projects.show', [$request->slug]); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return Response
     */
    public function destroy(Project $project)
    {
        //
    }
}
