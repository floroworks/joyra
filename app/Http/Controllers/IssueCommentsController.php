<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CommentRequest;

use App\Jobs\CreateCommentJob;

use App\Comment;
use App\Issue;

use Bandit\Repositories\UserRepositoryInterface;

class IssueCommentsController extends Controller
{

    // temp
    protected $user;

    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CommentRequest  $request
     * @param  App\Issue $issue
     * @return Response
     */
    public function store(CommentRequest $request, $issue)
    {
        $this->dispatchFrom(CreateCommentJob::class, $request);

        return redirect()->route('issues.show', [$issue->slug]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Comment  $comment
     * @param  App\Issue    $issue
     * @return Response
     */
    public function edit(Issue $issue, Comment $comment)
    {

        //temp
        $users = $this->user->getAllUsersAsListOrderedBy('name')->all();

        return view('comments.edit', compact('comment', 'issue', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CommentRequest  $request
     * @param  App\Issue       $issue
     * @param  App\Comment     $comment
     * @return Response
     */
    public function update(CommentRequest $request, Issue $issue, Comment $comment)
    {
        $this->dispatchFrom(CreateCommentJob::class, $request);

        return redirect()->route('issues.show', [$issue->slug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
