<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Event;

use App\Issue;
use App\Project;
use App\IssueType;
use App\User;

use App\Http\Requests\CreateIssueRequest;
use App\Jobs\CreateIssueJob;
use App\Jobs\CreateCommentJob;

use App\Events\IssueCreatedEvent;


use Bandit\Repositories\UserRepositoryInterface;
use Bandit\Repositories\IssuesRepositoryInterface;
use Bandit\Repositories\ProjectRepositoryInterface;
use Bandit\Repositories\WorkflowRepositoryInterface;
use Bandit\Repositories\ActionEditableFieldRepositoryInterface;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Attachment;


class IssuesController extends Controller
{
    protected $user;
    protected $issue;

    public function __construct(
            IssuesRepositoryInterface $issue, 
            UserRepositoryInterface $user)
    {
        $this->user = $user;
        $this->issue = $issue;

       // $this->middleware('auth', ['except' => ['show']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $issues = $this->issue->getAllIssuesWith(
            ['project', 
             'issueType', 
             'createdBy', 
             'assignedTo'
            ]);

        return view('issues.index', compact('issues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {                           
        return view('issues.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateIssueRequest  $request
     * @return Response
     */
    public function store(CreateIssueRequest $request)
    {
        $slug = $this->dispatchFrom(CreateIssueJob::class, $request);

        $issue = Issue::where('slug', $slug)->first();

        //dd($issue);

        // Files
        $attachments = Input::file('attachments');
        //dd($attachments);
        $this->processAttachments($attachments, $issue->id);

        //Event::fire(new IssueCreatedEvent($issue));
      
        return redirect()->route('issues.show', [$slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Issue  $issue
     * @return Response
     */
    public function show(Issue $issue)
    {
        // get the comments of this issue
        $comments = \App\Comment::where('issue_id', $issue->id)->with('user')->get();
        $historys = \App\History::where('issue_id', $issue->id)->get();
        // temporarily send users
        $users = $this->user->getAllUsersAsListOrderedBy('name')->all();

        // Attachments
        $attachments = Attachment::where('issue_id', $issue->id)->get();

        return view('issues.show', compact('issue', 'comments', 'users', 'historys', 'attachments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Issue  $issue
     * @return Response
     */
    public function edit(Issue $issue)
    {
        
        // Get the available user to assign to
       // $users = $this->user->getAllUsersAsListOrderedBy('name')->all();

        return view('issues.edit', compact('issue'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CreateIssueRequest  $request
     * @param  int  $id ** don't need this... keep for now
     * @return Response
     */
    public function update(CreateIssueRequest $request, $id)
    {
        $slug = $this->dispatchFrom(CreateIssueJob::class, $request);

        $issue = Issue::where('slug', $slug)->first();

        // Files
        $attachments = Input::file('attachments');
        //dd($attachments);
        $this->processAttachments($attachments, $issue->id);


        if ($request->has('comment')) {
            $this->dispatchFrom(CreateCommentJob::class, $request);
        }

        return redirect()->route('issues.show', [$slug]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    private function processAttachments($attachments, $issue_id)
    {
        if (!is_null($attachments[0])) {
            foreach ($attachments as $attachment) {
                $extension = $attachment->getClientOriginalExtension();
                Storage::disk('local')->put($attachment->getFilename() . '.' . $extension,
                                File::get($attachment));
                $attachment_record = new Attachment();
                $attachment_record->issue_id = $issue_id;
                $attachment_record->display_filename = $attachment->getClientOriginalName();
                $attachment_record->mime_type = $attachment->getClientMimeType();
                $attachment_record->full_path = $attachment->getFilename() . '.' . $extension;
                //dd($attachment_record);
                $attachment_record->save();
            }
        }        
    }
}
