<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\IssueTypeRequest;

class IssueTypeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.superadmin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $issue_types = \App\IssueType::all();
        return view('admin.workflows.issuetypes', compact('issue_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(IssueTypeRequest $request, \App\IssueType $issue_type)
    {
        
        \App\IssueType::create($request->all());

        return redirect()->route('admin.issue_types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(IssueTypeRequest $request, \App\IssueType $issue_type)
    {       
        //dd($issue_type); 
        $issue_type->update($request->all());
        return redirect()->route('admin.issue_types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
