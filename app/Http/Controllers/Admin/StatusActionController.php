<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StatusActionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.superadmin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.workflows.statusactions');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $status_action = \App\StatusAction::create($request->all());

        //$status_action->editableFields()->sync($request->editable_fields);
        $this->dispatch(new \App\Jobs\AddEditableFieldsToActionJob($status_action, $request->editable_fields));

        return redirect()->route('admin.status_actions.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, \App\StatusAction $status_action)
    {
        $status_action->update($request->all());

        //$status_action->editableFields()->sync($request->editable_fields);
        //dd($status_action);

        $this->dispatch(new \App\Jobs\AddEditableFieldsToActionJob($status_action, $request->editable_fields));


        return redirect()->route('admin.status_actions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(\App\StatusAction $status_action)
    {
        //dd($status_action);
        //$status_action->delete();

        $this->dispatch(new \App\Jobs\DeleteStatusActionJob($status_action));

        return redirect()->route('admin.status_actions.index');
    }
}
