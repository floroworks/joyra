<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WorkflowController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.superadmin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $statuses = \App\IssueStatusType::with('actions')->get();
        $status_options = \App\IssueStatusType::lists('status', 'id');
        $action_options = \App\StatusAction::lists('action', 'id');

        return view('admin.workflows.index', compact('statuses', 'status_options', 'action_options'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $status = \App\IssueStatusType::find($request->status_id);

        $this->dispatch(new \App\Jobs\AddWorkflowJob($status, $request->actions));

        return redirect()->route('admin.workflows.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, \App\IssueStatusType $status_type)
    {
        $this->dispatch(new \App\Jobs\AddWorkflowJob($status_type, $request->actions));
        //dd($status_type);

        return redirect()->route('admin.workflows.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
