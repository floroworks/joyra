<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\IssueStatusTypeRequest;

class IssueStatusTypeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.superadmin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $issue_status_types = \App\IssueStatusType::all();

        return view('admin.workflows.issuestatustypes', compact('issue_status_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(IssueStatusTypeRequest $request)
    {
        \App\IssueStatusType::create($request->all());

        return redirect()->route('admin.issue_status_types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(IssueStatusTypeRequest $request, \App\IssueStatusType $issue_status_type)
    {
        $issue_status_type->update($request->all());

        return redirect()->route('admin.issue_status_types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
