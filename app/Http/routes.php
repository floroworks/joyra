<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
|
*/

Event::listen('illuminate.query', function($sql)
{
// var_dump($sql);
});


Route::get('/', function () {
    return redirect('/home');
});

Route::get('/home', 'HomeController@index');


// Auth route
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Register
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::controllers(
    [
        'password' => 'Auth\PasswordController',
    ]
    );

Route::resource('projects', 'ProjectsController');
Route::model('projects', 'Project');

Route::bind('projects', function($value, $route) {
    return App\Project::whereSlug($value)->with('projectLead')->first();
});

Route::resource('issues', 'IssuesController');
Route::model('issues', 'Issue');

Route::bind('issues', function($value, $route) {
    return $this->app['Bandit\Repositories\IssuesRepositoryInterface']->getThisIssueWith($value, ['project', 
             'issueType', 
            'createdBy', 
            'assignedTo', 
            'issueStatusType',
            'severityType',
            'priorityType',
            'issueResolutionType']);
});

Route::resource('issues.comments', 'IssueCommentsController');
Route::model('comments', 'Comment');

Route::bind('comments', function($value, $route) {
    return $this->app['Bandit\Repositories\CommentRepositoryInterface']->getThisComment($value);
});


// Admin Area
Route::get('admin', function() {
    return redirect('admin/users');
});


Route::resource('admin/users', 'Admin\UserController');

Route::model('users', 'App\User');
Route::bind('admin/users', function($value, $route) {
  //dd(hello);
  return $this->app['Bandit\Repositories\UserRepositoryInterface']->getUserById($value);
});

Route::resource('admin/workflows', 'Admin\WorkflowController');
Route::model('workflows', 'App\IssueStatusType');

Route::resource('admin/issue_types', 'Admin\IssueTypeController');
Route::model('issue_types', 'App\IssueType');

Route::resource('admin/issue_status_types', 'Admin\IssueStatusTypeController');
Route::model('issue_status_types', 'App\IssueStatusType');

Route::resource('admin/issue_resolution_types', 'Admin\IssueResolutionTypeController');
Route::model('issue_resolution_types', 'App\IssueResolutionType');

Route::resource('admin/status_actions', 'Admin\StatusActionController');
Route::model('status_actions', 'App\StatusAction');

Route::bind('admin/workflows', function($value, $route) {
    //dd($value);
    return \App\IssueStatusType::find($value);
});

Route::get('assets/{id}', 'AssetController@show');