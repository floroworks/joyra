<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

use Bandit\Repositories\WorkflowRepositoryInterface;
use Bandit\Repositories\ActionEditableFieldRepositoryInterface;


class WorkflowComposer
{

    protected $workflow;
    protected $editable_field;

    public function __construct(WorkflowRepositoryInterface $workflow, ActionEditableFieldRepositoryInterface $editable_field)
    {
        $this->workflow = $workflow;
        $this->editable_field = $editable_field;
    }

    public function compose(View $view)
    {
        //dd($view->getData());
        $issue = $view->getData()['issue'];
        //dd($issue);
        $workflow_actions = $this->workflow->getActionsAvailableFor($issue->issue_status_type_id);

        $workflows = array();
        foreach ($workflow_actions as $wf) {
 
            $fields_obj = $this->editable_field->getEditableFieldsForAction($wf->id);
            $fields = array();
            $fields_obj->each(function ($field) use (&$fields) {
                $fields[] = $field->field_name;
            });

            $workflows[] = ['action' => $wf->action, 
                            'action_id' => preg_replace('/\s+/', '', $wf->action), 
                            'resulting_status_id' => $wf->resulting_status_id,
                            'fields' => $fields];            
        }
        //dd($workflows);
        $view->with('workflows', $workflows);
    }

}