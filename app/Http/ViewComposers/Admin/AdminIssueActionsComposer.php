<?php namespace App\Http\ViewComposers\Admin;

use Illuminate\Contracts\View\View;


class AdminIssueActionsComposer
{

    public function compose(View $view)
    {

        //dd('hello');

        $status_actions = \App\StatusAction::with('resultingStatus', 'editableFields')->get();

        $status_options = \App\IssueStatusType::lists('status', 'id');

        $editable_fields = \App\EditableIssueField::lists('display_name', 'id');

        $view->with('status_actions', $status_actions);
        $view->with('status_options', $status_options);
        $view->with('editable_fields', $editable_fields);

    }

}