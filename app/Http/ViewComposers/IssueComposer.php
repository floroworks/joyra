<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

use Bandit\Repositories\IssuesRepositoryInterface;
use Bandit\Repositories\ProjectRepositoryInterface;

class IssueComposer
{

    protected $issue;
    protected $project;

    public function __construct(IssuesRepositoryInterface $issue, ProjectRepositoryInterface $project)
    {
        $this->issue = $issue;
        $this->project = $project;
    }

    public function compose(View $view)
    {
        $view->with('issue_type_options', $this->issue->getIssueTypesAsList());
        $view->with('issue_status_type_options', $this->issue->getIssueStatusTypesAsList());
        $view->with('issue_resolution_type_options', $this->issue->getIssueResolutionTypesAsList());
        $view->with('project_options', $this->project->getProjectsAsList());
        $view->with('issue_priority_options', $this->issue->getPriorityTypesAsList());
        $view->with('issue_severity_options', $this->issue->getSeverityTypesAsList());
    }
}