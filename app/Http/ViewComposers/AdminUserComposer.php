<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

use App\User;
use Bandit\Repositories\UserRepositoryInterface;

class AdminUserComposer
{

    protected $user;

    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }

    public function compose(View $view)
    {
        $view->with('users', $this->user->getAllUsersWith(['roles']));
        //$view->with('users_and_roles', $this->user->getAllUsersWithRoles());
    }
}