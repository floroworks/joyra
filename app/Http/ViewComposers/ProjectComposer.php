<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

use Bandit\Repositories\ProjectRepositoryInterface;

class ProjectComposer
{

    protected $project;

    public function __construct(ProjectRepositoryInterface $project)
    {
        $this->project = $project;
    }

    public function compose(View $view)
    {
        $view->with('projects', $this->project->getProjectsWithProjectLeads());
    }
}