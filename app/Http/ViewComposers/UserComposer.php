<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

use App\User;
use Bandit\Repositories\UserRepositoryInterface;

class UserComposer
{

    protected $user;

    public function __construct(UserRepositoryInterface $user)
    {
        //dd($user);
        $this->user = $user;
    }

    public function compose(View $view)
    {
        $view->with('users', $this->user->getAllUsersAsListOrderedBy('name'));
    }
}