<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Issue;

class IssueUpdatedEvent extends Event
{
    use SerializesModels;

    public $issue;
    public $changed_columns;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Issue $issue, array $changed_columns)
    {
        $this->issue = $issue;
        $this->changed_columns = $changed_columns;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
