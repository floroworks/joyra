<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EditableIssueField extends Model
{
    public function actions()
    {
        return $this->belongsToMany('App\StatusAction', 'action_edit_fields', 'editable_field_id', 'action_id');
    }
}
