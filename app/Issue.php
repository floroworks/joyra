<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Event;

class Issue extends Model
{
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::created(function ($issue) {
            Event::fire('issue.created', $issue);
        });

        static::updated(function ($issue) {
            Event::fire('issue.updated', $issue);
        });
    }

    public function project()
    {
        return $this->belongsTo('App\Project', 'project_id');
    }

    public function issueType()
    {
        return $this->belongsTo('App\IssueType', 'issue_type_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function assignedTo()
    {
        return $this->belongsTo('App\User', 'assigned_to');
    }

    public function issueStatusType()
    {
        return $this->belongsTo('App\IssueStatusType', 'issue_status_type_id');
    }

    public function issueResolutionType()
    {
        return $this->belongsTo('App\IssueResolutionType', 
                                'issue_resolution_type_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function priorityType()
    {
        return $this->belongsTo('App\PriorityType', 'priority_id');
    }

    public function severityType()
    {
        return $this->belongsTo('App\SeverityType', 'severity_id');
    }

    public function historys()
    {
        return $this->hasMany('App\History');
    }

    public function attachments()
    {
        return $this->hasMany('\App\Attachment');
    }


}
