<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IssueResolutionType extends Model
{

    protected $fillable = ['type', 'description'];

    public function issues()
    {
        $this->hasMany('App\Issue');
    }
}
