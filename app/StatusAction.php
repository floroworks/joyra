<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusAction extends Model
{

    protected $fillable = ['action', 'resulting_status_id'];

    public function status()
    {
        return $this->belongsToMany('App\IssueStatusType', 'workflows', 'available_action', 'current_status');
    }

    public function editableFields()
    {
        return $this->belongsToMany('App\EditableIssueField', 'action_edit_fields', 'action_id', 'editable_field_id');
    }

    public function resultingStatus()
    {
        return $this->belongsTo('App\IssueStatusType', 'id');
    }
}
