<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    protected $fillable = ['name', 'user_id', 'slug'];
    
    public function projectLead()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function issues()
    {
        return $this->hasMany('App\Issue');
    }
}
