<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectsCounter extends Model
{
    // explicitly specify the table name as it is not plural
    protected $table = 'projects_counter';
}
