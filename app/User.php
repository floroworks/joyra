<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract, 
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function project()
    {
        return $this->hasMany('App\Project');
    }

    public function createdIssues()
    {
        return $this->hasMany('App\Issue', 'created_by');
    }

    public function assignedIssues()
    {
        return $this->hasMany('App\Issue', 'assigned_to');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles', 'user_id', 'role_id');
    }

    public function isSuperAdmin()
    {
        foreach($this->roles as $role) {
            if ($role->role == 'superuser')
                return true;
        }

        return false;
    }

    public function isAdmin()
    {
        foreach($this->roles as $role) {
            if ($role->role == 'admin')
                return true;
        }

        return false;
    }

    public function isDev()
    {
        foreach($this->roles as $role) {
            if ($role->role == 'developers')
                return true;
        }

        return false;        
    }

    public function isUser()
    {
        foreach($this->roles as $role) {
            if ($role->role == 'user')
                return true;
        }

        return false;        
    }

    public function historys()
    {
        return $this->hasMany('App\History');
    }
}
