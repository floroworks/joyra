@extends('admin.layouts.master')

@section('content')
  <div class="container-fluid">
    <div class="row page-title-row">
      <div class="col-md-6">
        <h3>Users <small>» Listing</small></h3>
      </div>
      <div class="col-md-6 text-right">
        <a href="/admin/tag/create" class="btn btn-success btn-md" data-toggle="modal" data-target="#registerModal" data-title="Register">
          <i class="fa fa-plus-circle"></i> New User
        </a>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">



        <table id="tags-table" class="table table-striped table-bordered">
          <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th class="hidden-md">Roles</th>
            <th data-sortable="false">Actions</th>          
          </tr>
          </thead>
          <tbody>
            @foreach ($users as $user)
            <tr>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td>
              <td class="hidden-md">
                  <a href="#" class="btn btn-xs btn-{{ $user->roles->find(1) ? 'primary' : 'default' }}">super</a>
                  <a href="#" class="btn btn-xs btn-{{ $user->roles->find(2) ? 'primary' : 'default' }}">admin</a>
                  <a href="#" class="btn btn-xs btn-{{ $user->roles->find(3) ? 'primary' : 'default' }}">dev</a>
                  <a href="#" class="btn btn-xs btn-{{ $user->roles->find(4) ? 'primary' : 'default' }}">user</a>
              </td>
              <td>
                <a href="/admin/users/{{ $user->id }}/edit"
                   class="btn btn-xs btn-info">
                  <i class="fa fa-edit"></i> Edit
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection



<div class="modal fade" role="dialog" id="registerModal">
  <div class="modal-dialog" role="document">

@include('auth.partials.registerpanel', ['display_modal' => true])

  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@section('scripts')
<script type="text/javascript">
$(document).ready(function (){
 
  $('#registerModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var action = button.data('title');
    var modal = $(this);
    modal.find('.modal-title').text(action);
  })


});
</script>
@endsection
