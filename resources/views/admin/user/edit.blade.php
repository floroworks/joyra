@extends('admin.layouts.master')

@section('content')
<div class="col-md-8 col-md-offset-2">
<div class="panel panel-default">
    <div class="panel-heading">Edit User</div>
    <div class="panel-body">
        {!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'PATCH']) !!}
        <div class="form-group">
            <label>Name</label>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label>Email</label>
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label>Roles</label>
            <br>
            <label class="checkbox-inline">
                <input type="checkbox" name="roles[]" value="1" {{ in_array(1, $user->roles->pluck('id')->toArray()) ? "checked" : ""}}>
                super
                </input>
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="roles[]" value="2" {{ in_array(2, $user->roles->pluck('id')->toArray()) ? "checked" : ""}}>
                admin
                </input>
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="roles[]" value="3" {{ in_array(3, $user->roles->pluck('id')->toArray()) ? "checked" : ""}}>
                dev
                </input>
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="roles[]" value="4" {{ in_array(4, $user->roles->pluck('id')->toArray()) ? "checked" : ""}}>
                user
                </input>
            </label>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default">Update</button>
            <a href="/admin/users" class="btn btn-default">Cancel</a>
        </div>
        {!! Form::close() !!}
    </div>
</div>

</div>


@endsection