<ul class="nav navbar-nav">
  @if (Auth::check())
    <li @if (Request::is('admin/users*')) class="active" @endif>
      <a href="/admin/users">Users</a>
    </li>
    <li @if (Request::is('admin/workflows*')) class="active" @endif>
      <a href="/admin/workflows">Workflow</a>
    </li>
    <li @if (Request::is('admin/issue_types*')) class="active" @endif>
      <a href="/admin/issue_types">Issue Types</a>
    </li>
    <li @if (Request::is('admin/issue_status_types*')) class="active" @endif>
      <a href="/admin/issue_status_types">Issue Status Types</a>
    </li>
    <li @if (Request::is('admin/issue_resolution_types*')) class="active" @endif>
      <a href="/admin/issue_resolution_types">Issue Resolution Types</a>
    </li>
    <li @if (Request::is('admin/status_actions*')) class="active" @endif>
      <a href="/admin/status_actions">Issue Actions</a>
    </li>
    @endif
</ul>

<ul class="nav navbar-nav navbar-right">
  @if (Auth::guest())
    <li><a href="/auth/login">Login</a></li>
  @else
    <li><a href="/projects">Back to Joyra</a></li>
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
         aria-expanded="false">{{ Auth::user()->name }}
        <span class="caret"></span>
      </a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="/auth/logout">Logout</a></li>
      </ul>
    </li>
  @endif
</ul>