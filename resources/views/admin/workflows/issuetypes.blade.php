@extends('admin.layouts.master')

@section('content')
<div class="col-md-8 col-md-offset-2">
<div class="panel panel-default">
    <div class="panel-heading">Configure Issue Types</div>
    <div class="panel-body">
@if ($errors->admin_issue_type_request->all())
<div class="alert alert-danger">
<ul>
@foreach ($errors->admin_issue_type_request->all() as $error)
    <li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif        <table class="table">
            <thead>
                <tr>
                    <th>Issue Type</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($issue_types as $issue_type)
                {!! Form::model($issue_type, ['route' => ['admin.issue_types.update', $issue_type->id], 'method' => 'PATCH']) !!}
                <tr>
                    <td>{!! Form::text('type', null, ['class' => 'form-control']) !!}</td>
                    <td>{!! Form::text('description', null, ['class' => 'form-control']) !!}</td>
                    <td>
                        <button type="submit" class="btn btn-xs btn-default">Update</button>
                    </td>
                </tr>
                {!! Form::close() !!}
                @endforeach
                {!! Form::model(new App\IssueType, ['route' => ['admin.issue_types.store']]) !!}
                <tr>
                    <td>{!! Form::text('type', null, ['class' => 'form-control']) !!}</td>
                    <td>{!! Form::text('description', null, ['class' => 'form-control']) !!}</td>
                    <td>
                        <button type="submit" class="btn btn-xs btn-default">Add</button>
                    </td> 
                </tr>
                {!! Form::close() !!}
            </tbody>
        </table>
        
    </div>
</div>

</div>
@endsection