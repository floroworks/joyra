@extends('admin.layouts.master')

@section('content')
<div class="col-md-8 col-md-offset-2">
<div class="panel panel-default">
    <div class="panel-heading">Configure Issue Resolution Types</div>
    <div class="panel-body">
        
        <table class="table">
            <thead>
                <tr>
                    <th>Issue Resolution Type</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($issue_resolution_types as $issue_resolution_type)
                {!! Form::model($issue_resolution_type, ['route' => ['admin.issue_resolution_types.update', $issue_resolution_type->id], 'method' => 'PATCH']) !!}
                <tr>
                    <td>{!! Form::text('type', null, ['class' => 'form-control']) !!}</td>
                    <td>{!! Form::text('description', null, ['class' => 'form-control']) !!}</td>
                    <td>
                        <button type="submit" class="btn btn-xs btn-default">Update</button>
                    </td>
                </tr>
                {!! Form::close() !!}
                @endforeach
                {!! Form::model(new App\IssueResolutionType, ['route' => ['admin.issue_resolution_types.store']]) !!}
                <tr>
                    <td>{!! Form::text('type', null, ['class' => 'form-control']) !!}</td>
                    <td>{!! Form::text('description', null, ['class' => 'form-control']) !!}</td>
                    <td>
                        <button type="submit" class="btn btn-xs btn-default">Add</button>
                    </td> 
                </tr>
                {!! Form::close() !!}
            </tbody>
        </table>
        
    </div>
</div>

</div>
@endsection