@extends('admin.layouts.master')

@section('content')
<div class="col-md-8 col-md-offset-2">
<div class="panel panel-default">
    <div class="panel-heading">Configure Issue Status Types</div>
    <div class="panel-body">
        @include('errors.list')
        <table class="table">
            <thead>
                <tr>
                    <th>Issue Status Type</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($issue_status_types as $issue_status_type)
                {!! Form::model($issue_status_type, ['route' => ['admin.issue_status_types.update', $issue_status_type->id], 'method' => 'PATCH']) !!}
                <tr>
                    <td>{!! Form::text('status', null, ['class' => 'form-control']) !!}</td>
                    <td>{!! Form::text('description', null, ['class' => 'form-control']) !!}</td>
                    <td>
                        <button type="submit" class="btn btn-xs btn-default">Update</button>
                    </td>
                </tr>
                {!! Form::close() !!}
                @endforeach
                {!! Form::model(new App\IssueStatusType, ['route' => ['admin.issue_status_types.store']]) !!}
                <tr>
                    <td>{!! Form::text('status', null, ['class' => 'form-control']) !!}</td>
                    <td>{!! Form::text('description', null, ['class' => 'form-control']) !!}</td>
                    <td>
                        <button type="submit" class="btn btn-xs btn-default">Add</button>
                    </td> 
                </tr>
                {!! Form::close() !!}
            </tbody>
        </table>
        
    </div>
</div>

</div>
@endsection