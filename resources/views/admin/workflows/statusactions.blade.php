@extends('admin.layouts.master')

@section('content')
<div class="col-md-8 col-md-offset-2">
<div class="panel panel-default">
    <div class="panel-heading">Configure allowed actions and the resulting status type</div>
    <div class="panel-body">
        
        <table class="table">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>Resulting Status Type</th>
                    <th>Editable Fields</th>
                    <th>Actions (?)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($status_actions as $status_action)
                {!! Form::model($status_action, ['route' => ['admin.status_actions.update', $status_action->id], 'method' => 'PATCH']) !!}
                <tr>
                    <td>{!! Form::text('action', null, ['class' => 'form-control']) !!}</td>
                    <td>
                        {!! Form::select('resulting_status_id', $status_options, null, ['class' => 'form-control']) !!}
                    </td>
                    <td>
                        <ul class="list-group">
                        @foreach ($editable_fields as $id => $field)
                            <li class="list-group-item">
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="editable_fields[]" value="{{$id}}" {{ in_array($id, $status_action->editableFields->pluck('id')->toArray()) ? "checked" : ""}}>
                                        {{ $field }}
                                    </input>
                                </label>
                            </li>
                        @endforeach
                        </ul>
                    </td>
                    <td>
                        <button type="submit" class="btn btn-xs btn-default">Update</button>
                        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteActionModal" data-statusid="{{$status_action->id}}" data-actionname="{{$status_action->action}}">Delete</button>
                    </td>
                </tr>
                {!! Form::close() !!}
                @endforeach
                {!! Form::model(new App\StatusAction, ['route' => ['admin.status_actions.store']]) !!}
                <tr>
                    <td>{!! Form::text('action', null, ['class' => 'form-control']) !!}</td>
                    <td>{!! Form::select('resulting_status_id', $status_options, null, ['class' => 'form-control']) !!}</td>
                    <td>
                        <ul class="list-group">
                        @foreach ($editable_fields as $id => $field)
                            <li class="list-group-item">
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="editable_fields[]" value="{{$id}}">
                                        {{ $field }}
                                    </input>
                                </label>
                            </li>
                        @endforeach
                        </ul>
                    </td>
                    <td>
                        <button type="submit" class="btn btn-xs btn-default">Add</button>
                    </td> 
                </tr>
                {!! Form::close() !!}
            </tbody>
        </table>
        
    </div>
</div>

</div>
@endsection


@section('modals')
<div class="modal fade" id="deleteActionModal" tabindex="-1" role="dialog" aria-labelledby="deleteActionModalLabel">  
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Action</h4>
            </div>
            {!! Form::open(['route' => ['admin.status_actions.destroy'], 'method' => 'DELETE']) !!}
            <div class="modal-body">
                {!! Form::hidden('status_id') !!}
                <p>Are you sure you want to delete the action <strong></strong>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
            {!! Form::close() !!}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('scripts')
<script type="text/javascript">

$(document).ready( function () {
$('#deleteActionModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var status_id = button.data('statusid') // Extract info from data-* attributes
  var action_name = button.data('actionname')
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  //modal.find('.modal-title').text('Delete ' + action_name + ' action')
  modal.find('.modal-content form').attr('action','{!! route('admin.status_actions.index') !!}' + '/' + status_id)
  modal.find('.modal-body input').val(status_id)
  modal.find('.modal-body p strong').text(action_name)
})
})
</script>
@endsection