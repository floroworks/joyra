@extends('admin.layouts.master')

@section('content')
<div class="col-md-8 col-md-offset-2">
<div class="panel panel-default">
    <div class="panel-heading"><h3 class="panel-title">Configure Workflow</h3></div>
    <div class="panel-body">       
        <table class="table">
            <thead>
                <tr>
                    <th>If Current Status Is</th>
                    <th>Available Actions Are</th>
                    <th>Actions (?)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($statuses as $status)
                {!! Form::model($status, ['route' => ['admin.workflows.update', $status->id], 'method' => 'PATCH']) !!}
                <tr>
                    <td>
                        <select class="form-control" disabled="disabled">
                            @foreach($status_options as $key => $value)
                                <option value="{{ $key }}" {{ ($key === $status->id) ? "selected=selected" : ""  }}>{{ $value }}</option>
                            @endforeach
                        </select>
                        {!! Form::hidden('id', $status->id) !!}
                    </td>
                    <td>
                        <ul class="list-group">
                        @foreach ($action_options as $id => $action)
                            <li class="list-group-item">
                                <label class="checkbox-inline">
                                <input type="checkbox" name="actions[]" value="{{$id}}" {{ in_array($id, $status->actions->pluck('id')->toArray()) ? "checked" : ""}}>
                                    {{ $action }}
                                </input>
                                </label>
                            </li>
                        @endforeach
                        </ul>
                    </td>
                    <td>
                        <button type="submit" class="btn btn-xs btn-default">Update</button>
                    </td>
                </tr>
                {!! Form::close() !!}
                @endforeach
            </tbody>
        </table>
    </div>
</div>

</div>
@endsection