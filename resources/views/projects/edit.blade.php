@extends('layouts.master')

@section('content')

<h2>Edit Project</h2>

@include('errors.list')


{!! Form::model($project, 
        ['method' => 'PATCH', 'route' => ['projects.update', $project->slug]]
    ) 
!!}
@include('projects.partials._form', array('submitButtonText' => 'Update Project'))
{!! Form::close() !!}

@endsection