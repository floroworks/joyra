@extends('layouts.master')

@section('content')

<h2>Create New Project</h2>

@include('errors.list')

{!! Form::model(new App\Project, array('route' => ('projects.store'))) !!}
@include('projects.partials._form', array('submitButtonText' => 'Create Project'))
{!! Form::close() !!}

@endsection