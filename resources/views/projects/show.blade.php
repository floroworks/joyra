@extends('layouts.master')

@section('content')
<h2>{{ $project->name }}</h2>
<small>
    <strong>KEY: </strong>{{ $project->slug }}
    &nbsp;&nbsp;
    <strong>Lead: </strong>{{ $project->projectLead->name }}
</small>
<br>
@can ('update', $project)
<div>
    <a href="{!! route('projects.edit', array($project->slug)) !!}">
        Edit
    </a>
</div>
@endcan

@can ('create', new \App\Issue)
<div>
    <a href="{!! route('issues.create') !!}">
        Create New Issue
    </a>
</div>
@endcan

<h2>Issues</h2>
<table class="table">
    <thead>
        <tr>
            <th>Issue Key</th>
            <th>Summary</th>
            <th>Asignee</th>
            <th>Reporter</th>
            <th>Created</th>
            <th>Updated</th>
        </tr>
    </thead>
    <tbody>
        @if ( !$issues->count() )
        <tr>
            <td colspan="6">No Issues Logged</td>
        </tr>
        @else
        @foreach( $issues as $issue )
        <tr>
            <td>
                <a href="{{ route('issues.show', $issue->slug)}}">
                    {{ $issue->slug }}
                </a>
            </td>
            <td>{{ $issue->subject }}</td>
            <td>{{ $issue->assignedTo->name }}</td>
            <td>{{ $issue->createdBy->name }}</td>
            <td>{{ $issue->created_at }}</td>
            <td>{{ $issue->updated_at }}</td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>

<h2>Archived Issues</h2>
<table class="table">
    <thead>
        <tr>
            <th>Issue Key</th>
            <th>Summary</th>
            <th>Asignee</th>
            <th>Reporter</th>
            <th>Created</th>
            <th>Updated</th>
        </tr>
    </thead>
    <tbody>
        @if ( !$issues->count() )
        <tr>
            <td colspan="6">Nothing archived</td>
        </tr>
        @else
        @foreach( $issues_archived as $issue )
        <tr>
            <td>
                <a href="{{ route('issues.show', $issue->slug)}}">
                    {{ $issue->slug }}
                </a>
            </td>
            <td>{{ $issue->subject }}</td>
            <td>{{ $issue->assignedTo->name }}</td>
            <td>{{ $issue->createdBy->name }}</td>
            <td>{{ $issue->created_at }}</td>
            <td>{{ $issue->updated_at }}</td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>

@endsection