{!! Form::hidden('id', null) !!}

<div class="form-group">
    {!! Form::label('name', 'Project Name', array('class' => '')) !!}
    {!! Form::text('name', null, array('class' => 'form-control')) !!}
</div>

<div class="form-group">
    {!! Form::label('slug', 'Key', array('class' => '')) !!}
    {!! Form::text('slug', null, array('class' => 'form-control')) !!}
</div>

<div class="form-group">
    {!! Form::label('user', 'Project Lead', array('class' => '')) !!}
    {!! Form::select('user_id', $users, null, array('class' => 'form-control')) !!}
</div>

<div class="">
    {!! Form::submit($submitButtonText, array('class' => 'btn btn-default')) !!}
</div>
