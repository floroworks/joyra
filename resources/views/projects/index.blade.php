@extends('layouts.master')

@section('content')
<h2>All Projects</h2>
@can ('create', new \App\Project)
<div>
    {!! 
        link_to_route('projects.create', 
                      'Create New Project', 
                      [], 
                      array('class' => '')) 
    !!}
</div>
@endcan

@if ( !$projects->count() )
    You have no projects
@else
    <table class="table">
        <thead>
            <tr>
                <th>Project</th>
                <th>Key</th>
                <th>Project Lead</th>
            </tr>
        </thead>
        <tbody>
        @foreach( $projects as $project ) 
            <tr>
                <td>
                    <a href="{{ route('projects.show', $project->slug) }}">
                        {{ $project->name }}
                    </a>
                </td>
                <td>{{ $project->slug }}</td>
                <td>{{ $project->projectLead->name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>    
@endif


@endsection