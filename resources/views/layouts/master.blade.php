<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
@yield('title')

{!! HTML::style('css/app.css') !!}



@yield('css')

</head>
<body> 
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" 
                    data-toggle="collapse" 
                    data-target="#bs-example-navbar-collapse-1" 
                    aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/home">Joyra</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" 
                    id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="{!! route('projects.index') !!}">
                            Projects <span class="sr-only">(current)</span>
                        </a>
                    </li>
                </ul>
                 <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" 
                            placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">
                        Submit
                    </button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                    <li><a href="/auth/login">Login</a></li>
                    @else
                    @if (Auth::user()->isSuperAdmin())
                    <li>
                        <a href="/admin">Manage Joyra</a>
                    </li>
                    @endif
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                         aria-expanded="false">{{ Auth::user()->name }}
                        <span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="/auth/logout">Logout</a></li>
                      </ul>
                    </li>
                  @endif

                </ul>
                <ul>

                </ul>
            </div>
        </div>
    </nav>    

    <div class="container">
<!--div class="col-md-9"-->


@yield('content')


<!--/div-->
</div>
{!! HTML::script('js/jquery.min.js') !!}
{!! HTML::script('js/bootstrap.min.js') !!}

 
  
@yield('scripts')

@yield('modals')


</body>
</html>
