<h4>Edit Comment</h4>
@include('errors.list')
{!! Form::model($comment, array('route' => array('issues.comments.update', $issue->slug, $comment->id), 'method' => 'PATCH' )) !!}
{!! Form::hidden('id', $comment->id) !!}
{!! Form::hidden('issue_id', $issue->id) !!}
{!! Form::select('user_id', $users, null, array('class' => '')) !!}
<br>
{!! Form::textArea('comment') !!}
<br>
{!! Form::submit('Edit Comment') !!}
{!! Form::close() !!}