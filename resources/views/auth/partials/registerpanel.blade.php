<div class="container-fluid">
    <div class="row">
        <div class="{{ $display_modal ? '' : 'col-md-8 col-md-offset-2'}}">
            <div class="{{ $display_modal ? 'modal-content' : 'panel panel-default' }}">
                @if ($display_modal)
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Register</h4>
                </div>
                @else
                <div class="panel-heading">Register</div>
                @endif
                <div class="{{ $display_modal ? 'modal-body' : 'panel-body' }}">
                    @if ($display_modal)
                    <div class="container-fluid">
                    @endif
                    @include('errors.list')
                    <form 
                        class="form-horizontal" 
                        role="form" 
                        method="POST"
                        action="/auth/register"
                    >
                        <input 
                            type="hidden" 
                            name="_token"
                            value="{{ csrf_token() }}"
                        >
                        <div class="form-group">
                            <label 
                                class="col-md-4 control-label"
                            >
                                Name
                            </label>
                            <div class="col-md-6">
                                <input 
                                    type="text" 
                                    class="form-control"
                                    name="name"
                                    value="{{ old('name') }}"
                                >
                            </div>
                        </div>

                        <div class="form-group">
                            <label 
                                class="col-md-4 control-label"
                            >
                                E-Mail Address
                            </label>
                            <div class="col-md-6">
                                <input 
                                    type="email"
                                    class="form-control"
                                    name="email"
                                    value="{{ old('email') }}"
                                >
                            </div>
                        </div>

                        <div class="form-group">
                            <label 
                                class="col-md-4 control-label"
                            >
                                Password
                            </label>
                            <div class="col-md-6">
                                <input 
                                    type="password"
                                    class="form-control"
                                    name="password"
                                >
                            </div>
                        </div>

                        <div class="form-group">
                            <label 
                                class="col-md-4 control-label"
                            >
                                Confirm Password
                            </label>
                            <div class="col-md-6">
                                <input 
                                    type="password"
                                    class="form-control"
                                    name="password_confirmation"
                                >
                            </div>
                        </div>

                        @if (! $display_modal)
                        <div class="form-group">
                            <div 
                                class="col-md-6 col-md-offset-4"
                            >
                                <button 
                                    type="submit"
                                    class="btn btn-primary"
                                >
                                    Register
                                </button>
                            </div>
                        </div>
                        @endif
                    
                    @if ($display_modal)
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Register</button>
                    </div>
                    @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>