@extends('layouts.master')

@section('content')
<h4>
    <a href="{{ route('projects.show', $issue->project->slug) }}">
        {{ $issue->project->name }}
    </a>
    /
    <a href="{{ route('issues.show', $issue->slug) }}">
        {{ $issue->slug }}
    </a>
</h4>
<h2>{{ $issue->subject }}</h2>
@can ('update', $issue)
    <div class="btn-group" role="group" aria-label="Available issue actions">
        <a href="{{ route('issues.edit', $issue->slug) }}" class="btn btn-default">
            Edit
        </a>
    </div>
    <div class="btn-group" role="group" aria-label="Available issue actions">
    @foreach ($workflows as $workflow)
        @if (count($workflow['fields']) > 0)
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#{{ $workflow['action_id'] . 'Modal' }}" data-title="{{ $workflow['action'] }}">
            {{ $workflow['action'] }}
        </button>
        @endif
    @endforeach
    </div>
    <div class="btn-group" role="group" aria-label="Available issue actions">
    @foreach ($workflows as $workflow)
        @if (count($workflow['fields']) == 0)
        {!! Form::open(['route' => ['issues.update', $issue->slug], 'method' => 'PATCH', 'class' => 'btn']) !!}
        {!! Form::hidden('slug', $issue->slug) !!}
        {!! Form::hidden('issue_id', $issue->id) !!}
        {!! Form::hidden('issue_status_type_id', $workflow['resulting_status_id']) !!}
        {!! Form::hidden('assigned_to', Auth::user()->id) !!}
        <button type="submit" class="btn btn-default">{{ $workflow['action'] }}</button>
        {!! Form::close() !!}
        @endif
    @endforeach
    </div>
@endcan
<hr>

Type: {{ $issue->issueType->type }}
<br>
Priority: {{ $issue->priorityType->priority }}
<br>
Severity: {{ $issue->severityType->severity }}
<br>
Status: {{ $issue->issueStatusType->status }}
<br>
Resolution: {{ $issue->issueResolutionType->type }}
<br>
Reporter: {{ $issue->createdBy->name }}
<br>
Assignee: {{ $issue->assignedTo->name }}
<br>
Created: {{ $issue->created_at }}
<br>
Updated: {{ $issue->updated_at }}
<hr>
<h3>Attachments</h3>
<div>
@foreach ($attachments as $attachment)
<a href="/assets/{{ $attachment->id }}">{{ $attachment->display_filename }}</a>
<br>
@endforeach
</div>
<hr>
<h3>Description</h3>
{{ $issue->description }}
<hr>
<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">Comments</a></li>
    <li role="presentation"><a href="#history" aria-controls="history" role="tab" data-toggle="tab">History</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="comments">
@if ($comments->count())
    @foreach($comments->all() as $comment)
        <div>
            <h5>{{ $comment->user->name }} added a comment - {{ $comment->created_at }}</h5>
            @can ('update', $comment)
            <a href="{{ route('issues.comments.edit', array($issue->slug, $comment->id)) }}" class="text-right">Edit</a>
            @endcan
            <p> {{ $comment->comment }} </p>
            <hr>
        </div>
    @endforeach
@else
<div>
    <br>
    <p>No comments for this issue</p>
    <hr>
</div>
@endif

    </div>
    <div role="tabpanel" class="tab-pane" id="history">
    <br>
    @foreach($historys as $history)
    <p>{{ $history->change_description }}</p>
    <hr>
    @endforeach
    </div>
  </div>

</div>



@can ('create', new App\Comment)
<h4>Add Comments</h4>
@include('errors.list')
{!! Form::model(new App\Comment, array('route' => array('issues.comments.store', $issue->slug) )) !!}
{!! Form::hidden('id', null) !!}
{!! Form::hidden('issue_id', $issue->id) !!}
<br>
{!! Form::textArea('comment', null, array('class' => 'form-control')) !!}
<br>
{!! Form::submit('Submit Comment', array('class' => 'btn')) !!}
{!! Form::close() !!}
@endcan
@endsection

@can ('update', $issue)
@section('modals')
@include('issues.partials._actionmodal')
@endsection
@endcan