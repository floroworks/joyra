@extends('layouts.master')

@section('content')

<h2>Edit {{ $issue->slug }}</h2>

@include('errors.list')

{!! 
    Form::model($issue, array('route' => array('issues.update', $issue->slug), 'method' => 'PATCH', 'files' => true))
!!}
{!! Form::hidden('id', null) !!}
{!! Form::hidden('slug', $issue->slug) !!}
@include('issues.partials._form', array('submitButtonText' => 'Update Issue', 'action' => 'edit'))
{!! Form::close() !!}


@endsection