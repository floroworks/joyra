@extends('layouts.master')

@section('content')

<h2>Create New Issue</h2>

@include('errors.list')

{!! 
    Form::model(new App\Issue, array('route' => array('issues.store'), 'files' => true))
!!}
{!! Form::hidden('id', null) !!}
@include('issues.partials._form', array('submitButtonText' => 'Create Issue', 'action' => 'create'))
{!! Form::close() !!}

@endsection