@foreach ($workflows as $workflow)
@if (count($workflow['fields']) > 0)
<div class="modal fade" role="dialog" id="{{ $workflow['action_id'] . 'Modal' }}">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      {!! Form::open(['route' => ['issues.update', $issue->slug], 'method' => 'PATCH']) !!}
      {!! Form::hidden('slug', $issue->slug) !!}
      {!! Form::hidden('issue_id', $issue->id) !!}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Issue Action</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <!-- Status Section -->
              {!! Form::hidden('issue_status_type_id', $workflow['resulting_status_id']) !!}
            <!-- End of Status Section -->
            @if (in_array('resolution_type_id', $workflow['fields']))
            <!-- Resolution Section -->
              <label for="">Resolution</label>
              {!! Form::select('issue_resolution_type_id', $issue_resolution_type_options, null, ['class' => 'form-control']) !!}
            <!-- End of Resolution Section -->
            @endif
            @if (in_array('assigned_to', $workflow['fields']))
            <!-- Assignment Section -->
            <div class="form-group">
              <label for="">Asignee</label>
              {!! Form::select('assigned_to', $users, null, ['class' => 'form-control']) !!}
            </div>
            <!-- End of Assignment Section -->
            @else
            {!! Form::hidden('assigned_to', Auth::user()->id) !!}
            @endif
            <div class="form-group">
              <label for="">Comment</label>
              {!! Form::textArea('comment', null, ['class' => 'form-control']) !!}
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      {!! Form::close() !!}
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endif
@endforeach

@section('scripts')
<script type="text/javascript">
$(document).ready(function (){
  @foreach ($workflows as $workflow)
  @if (count($workflow['fields']) > 0)
  $('#{{ $workflow['action_id'] . 'Modal' }}').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var action = button.data('title');
    var modal = $(this);
    modal.find('.modal-title').text(action);
  })
  @endif
  @endforeach

});
</script>
@endsection
