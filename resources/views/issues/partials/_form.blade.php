<div class="form-group">
    {!! Form::label('project', 'Project', ['class' => '']) !!}
@if ($action == 'create')
    {!! 
        Form::select('project_id', 
                     $project_options, 
                     null, 
                     ['class' => 'form-control',
                     ]
        ) 
    !!}
@else
    {!! 
        Form::select('project_id', 
                     $project_options, 
                     null, 
                     ['class' => 'form-control',
                     'disabled' => 'disabled' 
                     ]
        ) 
    !!}
@endif
</div>
<div class="form-group">
    {!! Form::label('issue_type', 'Issue Type', ['class' => '']) !!}
    {!! 
        Form::select('issue_type_id', 
                     $issue_type_options, 
                     null, 
                     ['class' => 'form-control']
        ) 
    !!}
</div>
<div class="form-group">
    {!! Form::label('priority_id', 'Priority', ['class' => '']) !!}
    {!! Form::select('priority_id', 
                    $issue_priority_options,
                    null,
                    ['class' => 'form-control']
        ) 
    !!}
</div>
<div class="form-group">
    {!! Form::label('severity_id', 'Severity', ['class' => '']) !!}
    {!! Form::select('severity_id',
                     $issue_severity_options,
                     null,
                     ['class' => 'form-control']
        ) 
    !!}
</div>
<div class="form-group">
    {!! Form::label('issue_status_type', 'Issue Status', ['class' => ''])  !!}
    {!! Form::select('issue_status_type_id', 
                     $issue_status_type_options, 
                     null, 
                     ['class' => 'form-control']
        ) 
    !!}
</div>
<div class="form-group">
    {!! Form::label('issue_resolution_status_type', 'Issue Resolution', ['class' => '']) !!}
    {!! Form::select('issue_resolution_type_id',
                     $issue_resolution_type_options,
                     null,
                     ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('subject', 'Subject', ['class' => '']) !!}
    {!! Form::text('subject', null, ['class' => 'form-control']) !!}
</div>
@if (Auth::check() && $action == 'create')
{!! Form::hidden('created_by', Auth::user()->id) !!}
@else
<div class="form-group">
    {!! Form::label('created_by', 'Reporter', ['class' => '']) !!}
    {!! Form::select('created_by', $users, null, ['class' => 'form-control']) !!}
</div>
@endif
<div class="form-group">
    {!! Form::label('assigned_to', 'Assignee', ['class' => '']) !!}
    {!! Form::select('assigned_to', $users, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('attachments', 'Attachments', ['class' => '']) !!}
    {!! Form::file('attachments[]', ['multiple' => true, 'class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Description', ['class' => '']) !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
<div class="">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-default']) !!}
    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
</div>