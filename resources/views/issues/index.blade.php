@extends('layouts.master')

@section('content')

<h2>All Issues</h2>
@if ( !$issues->count() )
    No issues
@else
    <table class="table">
        <thead>
            <tr>
                <th>Issue ID</th>
                <th>Summary</th>
                <th>Description</th>
                <th>Issue Type</th>
                <th>Created by</th>
                <th>Assigned to</th>
                <th>Created at</th>
                <th>Last Updated</th>
            </tr>
        </thead>
        <tbody>
        @foreach ( $issues as $issue )
            <tr>
                <td>{{ $issue->slug }}</td>
                <td>{{ $issue->subject }}</td>
                <td>{{ $issue->description }}</td>
                <td>{{ $issue->issueType->type }}</td>
                <td>{{ $issue->createdBy->name }}</td>
                <td>{{ $issue->assignedTo->name }}</td>
                <td>{{ $issue->created_at }}</td>
                <td>{{ $issue->updated_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

@endsection