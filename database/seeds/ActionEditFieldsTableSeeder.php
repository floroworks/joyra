<?php

use Illuminate\Database\Seeder;

class ActionEditFieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('action_edit_fields')->delete();

        $fields = array(
            [
                'action_id' => '1',
                'editable_field_id' => '1',
            ],
            [
                'action_id' => '1',
                'editable_field_id' => '2',
            ],
            [
                'action_id' => '2',
                'editable_field_id' => '1',
            ],
            [
                'action_id' => '2',
                'editable_field_id' => '2',
            ],
            [
                'action_id' => '3',
                'editable_field_id' => '1',
            ],
            [
                'action_id' => '3',
                'editable_field_id' => '2',
            ],
            [
                'action_id' => '4',
                'editable_field_id' => '1',
            ],
            [
                'action_id' => '4',
                'editable_field_id' => '2',
            ],
            [
                'action_id' => '5',
                'editable_field_id' => '1',
            ],
            [
                'action_id' => '5',
                'editable_field_id' => '2',
            ],
            [
                'action_id' => '6',
                'editable_field_id' => '1',
            ],
            [
                'action_id' => '6',
                'editable_field_id' => '2',
            ],
        );
        
        DB::table('action_edit_fields')->insert($fields);
    }
}
