<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->delete();

        $faker = Faker\Factory::create();

        $comments = array();

        for($i = 0; $i < 100; $i++) {
            $comments[] = array(
                'issue_id' => $faker->numberBetween($min = 1, $max = 30),
                'user_id' => $faker->numberBetween($min = 3, $max = 22),
                'comment' => $faker->realText($maxNbChars = 200),
                'created_at' => new DateTime()
            );
        }

        DB::table('comments')->insert($comments);
    }
}
