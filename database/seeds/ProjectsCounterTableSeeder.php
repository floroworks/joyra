// /database/migrations/seeds/ProjectsCounterTable.php
<?php

use Illuminate\Database\Seeder;

class ProjectsCounterTableSeeder extends Seeder {

    public function run()
    {
        //DB::table('projects_counter')->delete();

        $projects_counter = array(
            [
                'project_id' => 1,
                'count' => 4,
            ],
            [
                'project_id' => 2,
                'count' => 3,
            ],
        );

        DB::table('projects_counter')->insert($projects_counter);
    }
}