<?php

use Illuminate\Database\Seeder;

class PriorityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('priority_types')->delete();

        $priorities = array(
                [
                    'priority' => 1,
                    'description' => 'Priority 1',
                ],
                [
                    'priority' => 2,
                    'description' => 'Priority 2',
                ],
                [
                    'priority' => 3,
                    'description' => 'Priority 3',
                ],
            );

        DB::table('priority_types')->insert($priorities);
    }
}
