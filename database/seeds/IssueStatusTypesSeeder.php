<?php

use Illuminate\Database\Seeder;

class IssueStatusTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('issue_status_types')->delete();

        $issue_status_types = array(
            [
                'status' => 'Open',
                'description' => 'Open issues',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'status' => 'In Progress',
                'description' => 'In Progress issues',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'status' => 'Resolved',
                'description' => 'Resolved issues',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'status' => 'On Hold',
                'description' => 'On Hold issues',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'status' => 'Closed',
                'description' => 'Closed status',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'status' => 'Reopened',
                'description' => 'Reopened status',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ]
        );

        DB::table('issue_status_types')->insert($issue_status_types);
    }
}
