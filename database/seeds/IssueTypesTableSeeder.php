// /database/seeds/IssueTypesTableSeeder.php
<?php

use Illuminate\Database\Seeder;

class IssueTypesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('issue_types')->delete();

        $issue_types = array(
            [
                'type' => 'Bug',
                'description' => 'Bugs are defects in the system.',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'type' => 'New Feature',
                'description' => 'New features or functionality',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'type' => 'Query',
                'description' => 'Items requiring clarification',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'type' => 'Improvement',
                'description' => 'Improvement on existing functionality',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
        );

        DB::table('issue_types')->insert($issue_types);
    }
}