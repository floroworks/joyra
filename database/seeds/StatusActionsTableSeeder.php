<?php

use Illuminate\Database\Seeder;

class StatusActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_actions')->delete();

        $status_actions = array(
            [
                'action' => 'Stop Progress',
                'resulting_status_id' => 1,
            ],
            [
                'action' => 'Start Progress',
                'resulting_status_id' => 2,
            ],
            [
                'action' => 'Resolve',
                'resulting_status_id' => 3,
            ],
            [
                'action' => 'Put On-Hold',
                'resulting_status_id' => 4,
            ],  
            [
                'action' => 'Close',
                'resulting_status_id' => 5,
            ], 
            [
                'action' => 'Reopen',
                'resulting_status_id' => 6,
            ],          
        );

        DB::table('status_actions')->insert($status_actions);
    }
}
