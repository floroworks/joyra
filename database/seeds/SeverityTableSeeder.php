<?php

use Illuminate\Database\Seeder;

class SeverityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('severity_types')->delete();

        $severities = array(
                [
                    'severity' => 'Low',
                    'description' => 'Low severity',
                ],
                [
                    'severity' => 'Med',
                    'description' => 'Med severity',
                ],
                [
                    'severity' => 'High',
                    'description' => 'High severity',
                ],
            );

        DB::table('severity_types')->insert($severities);
    }
}
