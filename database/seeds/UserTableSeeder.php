<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {

    public function run()
    {
        //DB::table('users')->delete();

        $faker = Faker\Factory::create();

        $users = array();

        $users = array(
                    [
                        'name' => 'Super User',
                        'email' => 'super@example.com',
                        'password' => bcrypt('password'),
                        'hidden' => true,
                    ],
                    [
                        'name' => 'Admin User',
                        'email' => 'admin@example.com',
                        'password' => bcrypt('password'),
                        'hidden' => true,
                    ],
                    [
                        'name' => 'Developer User',
                        'email' => 'dev@example.com',
                        'password' => bcrypt('password'),
                        'hidden' => false,
                    ],
                    [
                        'name' => 'Normal User',
                        'email' => 'normal@example.com',
                        'password' => bcrypt('password'),
                        'hidden' => false,
                    ],
                 );

        for ($i = 0; $i < 20; $i++) {
            $users[] = array(
                    'name' => $faker->name,
                    'email' => $faker->safeEmail,
                    'password' => bcrypt('password'),
                    'hidden' => false,
                );
        }




        DB::table('users')->insert($users);
    }
}