<?php

use Illuminate\Database\Seeder;

class IssuesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('issues')->delete();

        $faker = Faker\Factory::create();

        $issues = array();

        for ($i = 0; $i < 30; $i++) {
            $issues[] = array(
                'subject' => $faker->realText($maxNbChars = 60),
                'description' => $faker->realText($maxNbChars = 200, 
                                                  $indexSize = 2),
                'created_by' => $faker->numberBetween($min = 5, $max = 22),
                'assigned_to' => $faker->numberBetween($min = 5, $max = 22),
                'project_id' => $faker->numberBetween($min = 1, $max = 2),
                'issue_type_id' => $faker->numberBetween($min = 1, $max = 4),
                'issue_status_type_id' => $faker->numberBetween($min = 1, 
                                                                $max = 4),
                'issue_resolution_type_id' => $faker
                                                ->numberBetween($min = 1,
                                                                $max = 7),
                'priority_id' => $faker->numberBetween($min = 1, $max = 3),
                'severity_id' => $faker->numberBetween($min = 1, $max = 3),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            );
        }

        DB::table('issues')->insert($issues);
    }
}

