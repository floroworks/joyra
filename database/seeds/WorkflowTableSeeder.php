<?php

use Illuminate\Database\Seeder;

class WorkflowTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workflow = array(
            [
                'current_status' => 1,
                'available_action' => 2,
            ],
            [
                'current_status' => 1,
                'available_action' => 3,
            ], 
            [
                'current_status' => 1,
                'available_action' => 4,
            ],
            [
                'current_status' => 1,
                'available_action' => 5,
            ],  
            [
                'current_status' => 2,
                'available_action' => 1,
            ],
            [
                'current_status' => 2,
                'available_action' => 3,
            ], 
            [
                'current_status' => 2,
                'available_action' => 4,
            ],
            [
                'current_status' => 3,
                'available_action' => 6,
            ],
            [
                'current_status' => 3,
                'available_action' => 5,
            ], 
            [
                'current_status' => 4,
                'available_action' => 2,
            ],
            [
                'current_status' => 4,
                'available_action' => 5,
            ],
            [
                'current_status' => 5,
                'available_action' => 6,
            ],
            [
                'current_status' => 6,
                'available_action' => 5,
            ],
            [
                'current_status' => 6,
                'available_action' => 3,
            ],
            [
                'current_status' => 6,
                'available_action' => 4,
            ],
        
        );

        DB::table('workflows')->insert($workflow);
    }
}
