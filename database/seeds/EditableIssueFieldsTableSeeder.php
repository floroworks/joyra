<?php

use Illuminate\Database\Seeder;

class EditableIssueFieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('editable_issue_fields')->delete();

        $fields = array(
            [
                'field_name' => 'assigned_to',
                'display_name' => 'Assigned To Field',
            ],
            [
                'field_name' => 'resolution_type_id',
                'display_name' => 'Resolution Field',
            ],
        );  

        DB::table('editable_issue_fields')->insert($fields);
    }
}
