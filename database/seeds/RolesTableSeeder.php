<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $roles = array(
                [
                    'role' => 'superuser',
                ],
                [
                    'role' => 'admin',
                ],
                [
                    'role' => 'developers',
                ],                
                [
                    'role' => 'user',
                ]
            );

        DB::table('roles')->insert($roles);
    }
}
