<?php

use Illuminate\Database\Seeder;

class IssueResolutionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('issue_resolution_types')->delete();

        $issue_resolution_types = array(
            [
                'type' => 'Unresolved',
                'description' => 'Unresolved issues',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'type' => 'Fixed',
                'description' => 'Fixed issues',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'type' => 'Clarified',
                'description' => 'Clarified issues',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'type' => 'On Hold',
                'description' => 'On Hold issues',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'type' => 'Duplicate',
                'description' => 'Duplicate issues',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'type' => 'Cannot Reproduce',
                'description' => 'Cannot Reproduce issues',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'type' => 'Done',
                'description' => 'Done',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
        );

        DB::table('issue_resolution_types')->insert($issue_resolution_types);
    }
}
