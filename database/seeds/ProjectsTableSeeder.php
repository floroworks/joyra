// /database/migrations/seeds/ProjectsTableSeeder.php
<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder {

    public function run()
    {
        //DB::table('projects')->delete();

        $projects = array(
            [
                'id' => 1, 
                'name' => 'Project Cylon', 
                'slug' => 'CYL',
                'user_id' => 18,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'id' => 2, 
                'name' => 'Project Diamond', 
                'slug' => 'DMD',
                'user_id' => 13,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
        );

        DB::table('projects')->insert($projects);
    }
}