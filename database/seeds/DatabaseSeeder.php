<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(IssueStatusTypesSeeder::class);
        $this->call(IssueTypesTableSeeder::class);
        $this->call(IssueResolutionTypeTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(PriorityTableSeeder::class);
        $this->call(SeverityTableSeeder::class);
        $this->call(IssuesTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(StatusActionsTableSeeder::class);
        $this->call(WorkflowTableSeeder::class);
        $this->call(EditableIssueFieldsTableSeeder::class);
        $this->call(ActionEditFieldsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UserRolesTableSeeder::class);
        
        Model::reguard();
    }
}
