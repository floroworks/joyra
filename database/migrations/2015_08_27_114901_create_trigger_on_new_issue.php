<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerOnNewIssue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            'CREATE TRIGGER `before_issues_insert` 
                BEFORE INSERT ON `issues`
                    FOR EACH ROW 
                    BEGIN
                        DECLARE project_slug VARCHAR(40);
                        DECLARE project_count integer;
                        SET project_slug = 
                            (SELECT slug 
                             FROM projects 
                             WHERE projects.id = NEW.project_id);
                        SET project_count = 
                            (SELECT count 
                             FROM projects_counter 
                             WHERE 
                            projects_counter.project_id = NEW.project_id) + 1;
                        SET NEW.slug = 
                            CONCAT(project_slug, "-", project_count);
                    END'
        );

        DB::unprepared(
            'CREATE TRIGGER `after_issues_insert` 
                AFTER INSERT ON `issues`
                    FOR EACH ROW 
                    BEGIN
                        UPDATE projects_counter 
                        SET 
                        projects_counter.count = projects_counter.count + 1 
                        WHERE 
                        projects_counter.project_id = NEW.project_id;
                    END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `before_issues_insert`');
        DB::unprepared('DROP TRIGGER `after_issues_insert`');
    }
}
