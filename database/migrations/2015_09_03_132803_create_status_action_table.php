<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusActionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_actions', function (Blueprint $table) {
            $table->increments('id');
/*            $table->integer('status_id')->unsigned()->default(0);
            $table->foreign('status_id')->references('id')->on('issue_status_types');
*/          $table->string('action');
            $table->integer('resulting_status_id')->unsigned()->default(0);
            $table->foreign('resulting_status_id')->references('id')->on('issue_status_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('status_actions');
    }
}
