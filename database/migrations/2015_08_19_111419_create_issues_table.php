<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject', 255);
            $table->longtext('description');
            $table->string('slug');
            $table->integer('created_by')->unsigned()->default(0);
            $table->foreign('created_by')->references('id')->on('users');
            $table->integer('assigned_to')->unsigned()->default(0);
            $table->foreign('assigned_to')->references('id')->on('users');
            $table->integer('project_id')->unsigned()->default(0);
            $table->foreign('project_id')->references('id')->on('projects');
            $table->integer('issue_type_id')->unsigned()->default(0);
            $table->foreign('issue_type_id')
                    ->references('id')
                    ->on('issue_types');
            $table->integer('issue_status_type_id')->unsigned()->default(0);
            $table->foreign('issue_status_type_id')
                    ->references('id')
                    ->on('issue_status_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('issues');
    }
}
