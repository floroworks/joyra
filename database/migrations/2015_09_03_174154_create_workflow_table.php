<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkflowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('current_status')->unsigned()->default(0);
            $table->foreign('current_status')->references('id')->on('issue_status_types');
            $table->integer('available_action')->unsigned()->default(0);
            $table->foreign('available_action')->references('id')->on('status_actions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('workflow');
    }
}
