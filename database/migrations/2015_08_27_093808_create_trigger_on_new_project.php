<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerOnNewProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            'CREATE TRIGGER `after_project_insert` 
                AFTER INSERT ON `projects`
            FOR EACH ROW BEGIN
                INSERT 
                    INTO projects_counter (project_id, count) 
                    VALUES (NEW.id, 0);
            END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `after_project_insert`');
    }
}
