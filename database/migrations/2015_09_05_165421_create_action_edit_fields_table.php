<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionEditFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_edit_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('action_id')->unsigned()->default(0);
            $table->foreign('action_id')->references('id')->on('status_actions');
            $table->integer('editable_field_id')->unsigned()->default(0);
            $table->foreign('editable_field_id')->references('id')->on('editable_issue_fields');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('action_edit_fields');
    }
}
