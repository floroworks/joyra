<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrioritySeverityToIssue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('issues', function (Blueprint $table) {
            $table->integer('priority_id')->unsigned()->default(0);
            $table->foreign('priority_id')->references('id')->on('priority_types');
            $table->integer('severity_id')->unsigned()->default(0);
            $table->foreign('severity_id')->references('id')->on('severity_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issues', function (Blueprint $table) {
            //
        });
    }
}
